/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package decodingbinay;

/**
 *
 * @author ZANDUT
 */
public class DecodingEncoding
{
  
    public String BinaryToText(String binary)
    {
        String kata = "";
        binary = binary.replace(" ", "");
        for (int i = 0; i < binary.length(); )
        {
            String subs = binary.substring(i, i+8);
            int huruf = Integer.parseInt(subs, 2);
            kata += (char)huruf;
            i = i + 8;
        }
        
        return kata;
    }
    
    public String TextToBinary(String kata)
    {
        String kata1 = "";
        for (int i = 0; i < kata.length(); i++)
        {
            byte huruf = (byte)kata.charAt(i);
            String biner = Integer.toBinaryString(huruf);
            if (biner.length() % 8 != 0)
            {
                for (int j = 0; j < (biner.length() % 8); j++)
                {
                    biner = "0" + biner;
                }
            }
            kata1 += biner;
        }
        return kata1;
    }
    
    public String TextToHexa(String kata)
    {
        String kata1 = "";
        for (int i = 0; i < kata.length(); i++)
        {
            byte huruf = (byte)kata.charAt(i);
            String biner = Integer.toHexString(huruf);
            
            kata1 += biner;
        }
        return kata1;
    }
    
    public String BinaryToHexa(String binary)
    {
        String kata = "";
        binary = binary.replace(" ", "");
        for (int i = 0; i < binary.length(); )
        {
            String subs = binary.substring(i, i+8);
            int huruf = Integer.parseInt(subs, 2);
            kata += Integer.toHexString(huruf);
            i = i + 8;
        }
        
        return kata;
        
    }
    
    public String HexaToText(String hexa)
    {
        String kata = "";
        hexa = hexa.replace(" ", "");
        for (int i = 0; i < hexa.length(); )
        {
            String subs = hexa.substring(i, i+2);
            int huruf = Integer.parseInt(subs, 16);
            kata += (char)huruf;
            i = i + 2;
        }
        
        return kata;
    }
    
    public String HexaToBinary(String hexa)
    {
        String kata = "";
        hexa = hexa.replace(" ", "");
        for (int i = 0; i < hexa.length(); )
        {
            String subs = hexa.substring(i, i+2);
            int huruf = Integer.parseInt(subs, 16);
            kata += (char)huruf;
            i = i + 2;
        }
        
        return TextToBinary(kata);
    }
   
    
    
}
